import discord
from discord.ext import commands

import sys, traceback


def get_prefix(bot, message):

    prefixes = ['$']

    return commands.when_mentioned_or(*prefixes)(bot, message)

initial_extensions = ['core', 'commands.imagecmds', 'commands.source', 'commands.tools']

bot = commands.Bot(command_prefix=get_prefix, description='Shadow')

if __name__ == '__main__':
    for extension in initial_extensions:
        try:
            bot.load_extension(extension)
        except Exception as e:
            print(file=sys.stderr)
            traceback.print_exc()


@bot.event
async def on_ready():

    print('Loading Shadow')
    print(f'Started')

@bot.event
async def on_member_join(member):

    channel = discord.Object("426526121278832676")
    await bot.send_message(channel, ':wave: **Welcome {} to Scambaiting**'.format(member.mention))



bot.run('', bot=True, reconnect=True)
