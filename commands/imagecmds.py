import discord
from discord.ext import commands


class Images:

    def __init__(self, bot):
        self.bot = bot
        self.session = self.bot.http.session

    @commands.command(pass_context=True, aliases=['pug'])
    async def pugs(self, ctx):
        search = "http://pugme.herokuapp.com/random"
        try:
            await self.bot.send_typing(ctx.message.channel)
            async with self.session.get(search) as r:
                result = await r.json()
            url = result['pug']
            author = ctx.message.author.mention
            channel = ctx.message.channel.mention
            embed = discord.Embed(colour=discord.Colour(value=0x1afc51))
            embed.set_image(url='{}'.format(url))
            await self.bot.say('{}'.format(author), embed=embed)
            log = discord.Embed(description="``pug`` command was issued by {} in {}".format(author, channel), colour=discord.Colour(value=0x1afc51))
            log.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
            await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log)

        except:
            author = ctx.message.author.mention
            channel = ctx.message.channel.mention
            await self.bot.say(":x: {} **Sorry, i failed to run that command, will take the log home with me and see what went wrong**".format(author))
            log2=discord.Embed(description="``pug`` command was issued by {} in {} but failed :thinking:".format(author, channel), colour=discord.Colour(value=0xfc1d1a))
            log2.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
            await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log2)

    @commands.command(pass_context=True, aliases=['cat'])
    async def cats(self, ctx):

        search = "https://nekos.life/api/v2/img/meow"
        try:
            await self.bot.send_typing(ctx.message.channel)
            async with self.session.get(search) as r:
                result = await r.json()
            image = result['url']
            embed = discord.Embed(colour=discord.Colour(value=0x1afc51))
            embed.set_image(url='{}'.format(image))
            author = ctx.message.author.mention
            await self.bot.say('{}'.format(author), embed=embed)
            channel = ctx.message.channel.mention
            log3=discord.Embed(description="``cat`` command was issued by {} in {}".format(author, channel), colour=discord.Colour(value=0x1afc51))
            log3.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
            await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log3)

        except:
            author = ctx.message.author.when_mention
            channel = ctx.message.channel.mention
            await self.bot.say(":x: {} **Sorry, i failed to run that command, will take the log home with me and see what went wrong**".format(author))
            log4=discord.Embed(description="``cat`` command was issued by {} in {} but failed :thinking:".format(author, channel), colour=discord.Colour(value=0xfc1d1a))
            log4.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
            await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log4)

def setup(bot):
    n = Images(bot)
    bot.add_cog(n)
