import discord
from discord.ext import commands

class source:


    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def source(self, ctx):
        await self.bot.send_typing(ctx.message.channel)
        author = ctx.message.author.mention
        await self.bot.say(":information_source: {} **Here is the source code for me!**\n**https://gitlab.com/WoW-from-Discord/shadow**".format(author))
        channel = ctx.message.channel.mention
        log=discord.Embed(description="``source`` command was issued by {} in {}".format(author, channel), colour=discord.Colour(value=0x1afc51))
        log.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
        await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log)



def setup(bot):
    bot.add_cog(source(bot))
