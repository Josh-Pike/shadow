
import discord
from discord.ext import commands
import re


def process_avatar(url):
    if ".gif" in url:
        new_url = re.sub("\?size\=\d+$", "", url)
        return new_url
    else:
        new_url = url.replace('.webp', '.png')
        return new_url


class tools:
    """Get user's avatar URL."""

    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True, aliases=['pfp', 'Pfp', 'Avatar'])
    async def avatar(self, ctx, *, user: discord.Member=None):
        author = ctx.message.author

        if not user:
            user = author

        u = user.avatar_url
        url = process_avatar(u)
        channel = ctx.message.channel.mention
        author2 = ctx.message.author.mention
        embed = discord.Embed(colour=discord.Colour(value=0x1afc51))
        embed.set_image(url='{}'.format(url))
        await self.bot.say('{}'.format(author2), embed=embed)
        log = discord.Embed(description="``avatar`` command was issued by {} in {}".format(author2, channel), colour=discord.Colour(value=0x1afc51))
        log.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
        await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log)


def setup(bot):
    bot.add_cog(tools(bot))
