import discord
from discord.ext import commands
import datetime
import time
from random import choice, randint

class Core:


    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def ping(self, ctx):
        t1 = time.perf_counter()
        await self.bot.send_typing(ctx.message.channel)
        t2 = time.perf_counter()
        ping = str(round((t2-t1)*1000))
        author = ctx.message.author.mention
        channel = ctx.message.channel.mention

        await self.bot.say(":ping_pong: {} **this took me {}ms to send**".format(author, ping))

        log=discord.Embed(description="``ping`` command was issued by {} in {}".format(author, channel), colour=discord.Colour(value=0x1afc51))
        log.set_author(name='Issued Command', icon_url="https://i.imgur.com/uta2Ct8.png")
        await self.bot.send_message(discord.Object(id='469507067137490945'), embed=log)

    @commands.command(pass_context=True, name='reload', hidden=True)
    async def reload(self, ctx, *, command: str):

      if ctx.message.author.id =='269124996255711232':
        try:
            await self.bot.send_typing(ctx.message.channel)
            self.bot.unload_extension(command)
            self.bot.load_extension(command)
        except Exception as e:
            await self.bot.say(':x: **I failed to reload that command/mod**')
            author = ctx.message.author
            error = e
            embed = discord.Embed(description="```py\n{}```".format(error), colour=discord.Colour(value=0xfc1d1a))
            print(error)
            embed.set_author(name='Reload command error', icon_url="https://i.imgur.com/uta2Ct8.png")
            await self.bot.send_message(discord.Object(id='469507067137490945'), embed=embed)
        else:
            await self.bot.say(':thumbsup: **Command/Mod is online!**')



def setup(bot):
    bot.add_cog(Core(bot))
